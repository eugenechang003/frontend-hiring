# Project Questionnaire

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Task

Build a questionnaire that you would pass to your clients to learn what they want out of their new web project that you are going to work on. 

The questionnaire should help you to understand your client’s expectations and preferences. At the same time, the questionnaire should also give you a better picture of your client’s budget, timeline, and any other important factors that you need to keep in mind before kicking off a new project.

The project can be whatever, but ideally it is one of your dream projects you have always wanted to complete.

## Requirements
- The questionnaire form needs to have between 5 and 8 input fields of four different types - a text, a select, an image, and a checkbox group.
- All input fields should be required and validated.
- The questionnaire data needs to be stored upon its submission. Hint - the image can be stored as a string.
- Below the questionnaire form, all past submissions need to be listed.
- Both the questionaire design and libraries used to build it are to your preference.

## Available Scripts

In the project directory, you can run:
### `yarn install`
### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
