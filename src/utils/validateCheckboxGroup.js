const validateCheckboxGroup = (group) => {
  if (group.length === 0) {
    return 'You must select at least one feature';
  }
}

export default validateCheckboxGroup;