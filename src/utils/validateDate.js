const reFormat = /^\d{1,2}\/\d{1,2}\/\d{4}$/;

const validateDate = (date) => {
  // check format of date
  if (!reFormat.test(date)) {
    return 'Date is invalid, use mm/dd/yyyy format';
  }

  // Parse the date parts to integers for valid dates of the year
  var parts = date.split("/");
  var day = parseInt(parts[1], 10);
  var month = parseInt(parts[0], 10);
  var year = parseInt(parts[2], 10);

  // Check the ranges of month and year
  if(year < 1000 || year > 3000 || month === 0 || month > 12)
      return false;

  var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

  // Adjust for leap years
  if(year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0))
      monthLength[1] = 29;

  // Check the range of the day
  if (day <= 0 && day > monthLength[month - 1]) {
    return 'Date is invalid';
  }

  // Check date is at least a week away
  var today = Date.now();
  var suggestedDate = new Date(year, month-1, day, 0, 0, 0, 0); // month is 0 based..
  if (Math.round((suggestedDate-today)/(1000*60*60*24)) < 7) {
    return 'Date must be at least a week from today';
  }
};

export default validateDate;