const validateImageWeight = imageFile => {
  if (imageFile.size) {
    // Get image size in kilobytes
    const imageFileKb = imageFile.size / 1024;
    const maxWeight = 200000;
    if (imageFileKb > maxWeight) {
      return `Image size must be less or equal to ${maxWeight}kb. `;
    } else {
      return '';
    }
  } else {
    return 'Invalid file, try different file';
  }
};

const validateImageFormat = imageFile => {
  const mimeType = "image/jpg, image/jpeg, image/png";
  if (!mimeType.includes(imageFile.type)) {
    return `Image mime type must be ${mimeType}. `;
  }
  else {
    return '';
  }
};

const validateImageDimensions = imageFile => {
  const maxHeight = 1000;
  const maxWidth = 1000;
  const imageObject = new window.Image();
  imageObject.onload = () => {
    imageFile.width = imageObject.naturalWidth;
    imageFile.height = imageObject.naturalHeight;
  };
  if (imageFile.width > maxWidth) {
    return `Image width must be less or equal to ${maxWidth}px. `;
  } else if (imageFile.height > maxHeight) {
    return `Image height must be less or equal to ${maxHeight}px. `;
  } else {
    return '';
  }
};

export const validateImage = imageFile => {
  if (imageFile) {
    // validate image size
    const imageSize = validateImageWeight(imageFile);

    // validate format
    const imageFormat = validateImageFormat(imageFile);

    // validate dimensions
    const imageDim = validateImageDimensions(imageFile);

    return imageSize + imageFormat + imageDim;
  } else {
    return 'Please choose file';
  }
};