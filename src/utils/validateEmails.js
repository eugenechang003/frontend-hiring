const re = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

const validateEmails = (email) => {
  if (!re.test(email)) {
    return 'Email is invalid';
  }
};

export default validateEmails;