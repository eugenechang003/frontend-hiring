import validateDate from '../utils/validateDate';
import validateEmails from '../utils/validateEmails';

describe('email text input validation functions', () => {
  it('should accept when email is valid', () => {
    expect(validateEmails('yougenechang@gmail.com')).toEqual();
    expect(validateEmails('bobbrian@yahoo.com')).toEqual();
  });

  it('should reject when email is not valid', () => {
    expect(validateEmails('yougenechang.com')).toEqual('Email is invalid');
    expect(validateEmails('yougenechang.')).toEqual('Email is invalid');
  });
});

describe('date text input validation functions', () => {
  it('should accept when date is valid', () => {
    expect(validateDate('12/20/2022')).toEqual();
    expect(validateDate('7/20/2026')).toEqual();
  });

  it('should reject when date format is invalid', () => {
    expect(validateDate('13/12/5')).toEqual('Date is invalid, use mm/dd/yyyy format');
    expect(validateDate('7-20-2021')).toEqual('Date is invalid, use mm/dd/yyyy format');
  });

  it('should reject when date is invalid', () => {
    expect(validateDate('13/12/2025')).toEqual('Date is invalid');
  });


  it('should reject when date is too early', () => {
    expect(validateDate('12/20/2020')).toEqual('Date must be at least a week from today');
    expect(validateDate('7/7/2021')).toEqual('Date must be at least a week from today');
  });
});