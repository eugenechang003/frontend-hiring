import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';

const CheckboxGroup  = ({ input, label, options, type, meta: { error, touched } }) => {
  const {name, onChange, onBlur, onFocus} = input;
  const inputValue = input.value;

  const checkboxes = _.map(options, ({label, value}, index) => {
    const handleChange = (event) => {
      const arr = [...inputValue];
      if (event.target.checked) {
        arr.push(value);
      }
      else {
        arr.splice(arr.indexOf(value), 1);
      }
      onBlur(arr);
      return onChange(arr);
    };
    const checked = inputValue.includes(value);
    return (
      <div key={`checkbox-${index}`}>
        <label>
          <input
            type="checkbox"
            className='filled-in'
            name={`${name}[${index}]`}
            value={value}
            checked={checked}
            onChange={e => handleChange(e)}
            onFocus={onFocus}
          />
          <span>{label}</span>
        </label>
      </div>
    );
  });

  return (
    <div className='section'>
      <label className='flow-text'>{label}</label>
      <div style={{ marginBottom: '5px' }}>{checkboxes}</div>
      {touched && error && <p className="error" style={{ marginBottom:'20px', color: 'red' }}>{error}</p>}
    </div>
  );
};

CheckboxGroup.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  type: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
};

export default CheckboxGroup;