import React from 'react';
import PropTypes from 'prop-types';

const TextField = ({ input, label, meta: { error, touched } }) => {
  return (
    <div>
      <label className='flow-text'>{label}</label>
      <div className='input-field section'>
        <input {...input} style={{ marginBottom: '5px', textAlign: 'center' }}/>
        <div className="error" style={{ marginBottom:'20px', color: 'red' }}>
          {touched && error}
        </div>
      </div>
    </div>
  );    
};

TextField.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
};

export default TextField;