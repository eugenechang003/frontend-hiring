import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';

import { CLEAR_IMAGE, SET_IMAGE } from '../../actions/types';
import { validateImage } from '../../utils/validateImage';

const ImageField = ({ input, label, type, meta: { error, touched } }) => {
  const dispatch = useDispatch();
  const [errorMsg, setErrorMsg] = useState('');
  function onChange(e) {
    const { onChange, onBlur } = input;
    let fileReader;
    const handleFileRead = (e) => {
      const content = fileReader.result;
      dispatch({ type: SET_IMAGE, payload: content });
      setErrorMsg('');
    };

    const handleFileChosen = (file) => {
      const imageError = validateImage(file);
      if (imageError === '') {
        try {
          fileReader = new FileReader();
          fileReader.onloadend = handleFileRead;
          fileReader.readAsDataURL(file);
        } catch (e) {
          setErrorMsg(e);
          dispatch({ type: CLEAR_IMAGE });
        }
      } else {
        dispatch({ type: CLEAR_IMAGE });
        setErrorMsg(imageError);
      }
    };

    onBlur(e);
    return onChange(handleFileChosen(e.target.files[0]));
  }
  const { onFocus } = input;
  return (
    <div className='section'>
      <label className='flow-text'>{label}</label>
      <div>
        <input
          type={type}
          accept='.jpg, .png, .jpeg'
          onChange={e => onChange(e)}
          onFocus={onFocus}
        />
      </div>
      <div className="error" style={{ marginBottom:'20px', color: 'red' }}>
        {touched && errorMsg}
      </div>
    </div>
  );
};

ImageField.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  type: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
};

export default ImageField;