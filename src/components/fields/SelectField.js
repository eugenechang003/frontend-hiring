import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';

const SelectField = ({ input, label, options, meta: { error, touched } }) => {  
  return (
    <div className='input-field section'>
      <div className='row'><label className='flow-text'>{label}</label></div>
      <div>
        <select {...input} style={{ all:'unset' }}>
          <option disabled style={{ color: 'red' }}>Choose Range</option>
            {_.map(options, (op) => {
              return (
                <option key={`select-${op.value}`} value={op.value}>{op.label}</option>
              );
            })}
        </select>
        <div className="error" style={{ marginBottom:'20px', color: 'red' }}>
          {touched && error}
        </div>
      </div>
    </div>
  );    
};

SelectField.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  meta: PropTypes.object.isRequired,
};

export default SelectField;
