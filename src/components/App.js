import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../actions';
import Welcome from '../pages/Welcome';
import QuestionnaireForm from '../pages/QuestionnaireForm';
import Submissions from '../pages/Submissions';
import ThankYou from '../pages/ThankYou';

class App extends Component {
  render() {
    return (
      <div className='container center-align'>
        <BrowserRouter>
          <div>
            <Route exact path='/' component={Welcome} />
            <Route exact path='/questionnaire' component={QuestionnaireForm} />
            <Route exact path='/submissions' component={Submissions} />
            <Route exact path='/thankyou' component={ThankYou} />
          </div>
        </BrowserRouter>
      </div>
    )
  }
}

export default connect(null, actions)(App);