import React from 'react';
import { Link } from 'react-router-dom';

function Welcome() {
  return (
    <div className='container'>
      <h2>Welcome to Gainz Questionnaire</h2>
      <div className='collection'>
        <Link className='collection-item' to='/questionnaire'>Start</Link>
        <Link className='collection-item' to='/submissions'>View Submissions</Link>
      </div>
    </div>
  );
};

export default Welcome;