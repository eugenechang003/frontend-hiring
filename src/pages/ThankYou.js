import React from 'react';
import { Link } from 'react-router-dom';

function ThankYou() {
  return (
    <div className='container'>
      <h2>Thank you for your submission!</h2>
      <div className='collection'>
        <Link className='collection-item' to='/'>Home</Link>
        <Link className='collection-item' to='/submissions'>Submissions</Link>
      </div>
    </div>
  );
};

export default ThankYou;