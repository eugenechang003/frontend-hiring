import _, { toInteger } from 'lodash';
import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { featureOptions, audienceOptions } from '../data/formFields';

function Submissions() {
  const submitted = useSelector((state) => state.questionnaire.questionnaires);
  const fields = _.map(submitted, ({ contactEmail, userSupport, releaseDate, features, imageUrl, audience }, index) => {
    return (
      <li className='collection-item avatar' key={`questionnaire-${index}`}>
        <div className='section'>
          <img src={imageUrl} alt="" class="circle" />
          <span class="title">Contact Email: {contactEmail}</span>
          <p>
            Expected Release Date: {releaseDate} <br />
            Expected Max User Base: {userSupport}
          </p>
        </div>
        
        <div className='section'>
          <p>Desired Features</p>
          <ul>
            {_.map(features, (feature, index) => {
              return <div key={`feature-${index}`}>{featureOptions[toInteger(feature)-1].label}</div>;
            })}
          </ul>
        </div>

        <div className='section'>
          <p>Expected Audience</p>
          <ul>
            {_.map(audience, (group, index) => {
              return <div key={`audience-${index}`}>{audienceOptions[toInteger(group)-1].label}</div>;
            })}
          </ul>
        </div>
      </li>
    );
  });

  return (
    <div>
      <h2>All Submissions</h2>
      <div className='container'>
        <ul className='collection'>
          {fields}
        </ul>
      </div>
      <Link to='/'>
        <button className='button'>Home</button>
      </Link>
    </div>
  );
};

export default Submissions;