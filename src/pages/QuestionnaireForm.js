import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { Link, withRouter } from 'react-router-dom';

// question data
import { formFields } from '../data/formFields';

// fields
import TextField from '../components/fields/TextField';
import CheckboxGroup from '../components/fields/CheckboxGroup';
import SelectField from '../components/fields/SelectField';
import ImageField from '../components/fields/ImageField';

// validations
import validateEmails from '../utils/validateEmails';
import validateDate from '../utils/validateDate';
import validateCheckboxGroup from '../utils/validateCheckboxGroup';

// actions
import { clearImage, submitForm } from '../actions';


class QuestionnaireForm extends Component {
  componentDidMount() {
    this.props.clearImage();
  }

  renderFields() {
    return _.map(formFields, ({ label, type, name, options }) => {
      switch (type) {
        case 'file':
          return (
            <Field key={name} name={name} label={label} type={type} component={ImageField} />
          );
        case 'select':
          return (
            <Field key={name} name={name} label={label} options={options} type={type} component={SelectField} />
          );
        case 'checkbox':
          return (
            <Field key={name} name={name} label={label} options={options} type={type} component={CheckboxGroup} />
          );
        case 'text':
        case 'email':
        default:
          return (
            <Field key={name} name={name} label={label} type={type} component={TextField} />
          );
      }
    });
  }

  render() {
    const { history, formValues } = this.props;
    return (
      <div className='container'>
        <h2>Questionnaire Form</h2>
        <form
          onSubmit={() => {
            history.push('/thankyou');
            let values = formValues;
            values.imageUrl = this.props.imageUrl;
            this.props.submitForm(values);
          }}
        >
          <div className='row'>
            {this.renderFields()}
          </div>
          <div className='row'>
            <button type='submit' disabled={this.props.invalid || this.props.imageUrl === ''} className='btn waves-effect waves-light'>
              <i className='material-icons'>Submit</i>
            </button>
            <Link to='/' className='red btn-flat white-text'>
              <i className='material-icons'>Cancel</i>
            </Link>
          </div>
        </form>
    </div>
    )
  }
};

function validate(values) {
  const errors = {};
  errors.contactEmail = validateEmails(values.contactEmail || '');
  errors.releaseDate = validateDate(values.releaseDate || '');
  errors.features = validateCheckboxGroup(values.features || '');
  errors.audience = validateCheckboxGroup(values.audience || '');

  // validate that input is not empty
  _.each(formFields, ({ name, type }) => {
    if (!values[name] && type !== 'file') {
      errors[name] = 'You must provide a value';
    }
  });

  return errors;
}

function mapStateToProps(state) {
  return {
    imageUrl: state.image.imageUrl,
    formValues: state.form.questionnaireForm.values,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    submitForm: questionnaire => {
      dispatch(submitForm(questionnaire));
    },
    clearImage: () => {
      dispatch(clearImage());
    },
  }
}

QuestionnaireForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(QuestionnaireForm));

QuestionnaireForm = connect(mapStateToProps)(QuestionnaireForm);
export default reduxForm({
  validate, // validation function (runs on value change)
  form: 'questionnaireForm', // name of form
  fields: [ 'contactEmail', 'userSupport', 'releaseDate', 'features', 'audience' ],
})(QuestionnaireForm);