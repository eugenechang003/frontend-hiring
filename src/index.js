// eslint-disable-next-line no-unused-vars
import materializeCSS from 'materialize-css/dist/css/materialize.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import App from './components/App';
import reducers from './reducers';

const store = createStore(reducers, {});

ReactDOM.render(
  <Provider store={store}><App /></Provider>,
  document.querySelector('#root')
);
