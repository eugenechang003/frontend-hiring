export const featureOptions = [
  { value: '1', label: 'account creation' },
  { value: '2', label: '2 factor authentication' },
  { value: '3', label: 'cross-platform application' },
  { value: '4', label: 'ability to upload and store media' },
];

export const audienceOptions = [
  { value: '1', label: '0-3' },
  { value: '2', label: '4-10' },
  { value: '3', label: '11-20' },
  { value: '4', label: '21-40' },
  { value: '5', label: '41-60' },
  { value: '6', label: '61+' },
];

export const supportOptions = [
  { value: 100, label: '0-100' },
  { value: 1000, label: '101-1000' },
  { value: 10000, label: '1001-10000' },
];

// formFields description
// type: determines what type of input
// label: question being asked
// name: input name (how redux-form maps key to value)
// options: choices for either checkbox or select inputs
export const formFields = [
  {
    type: 'email',
    label: 'What is your email address?',
    name: 'contactEmail',
    options: [],
  },
  {
    type: 'select',
    label: 'How many users would you like to support?',
    name: 'userSupport',
    options: supportOptions,
  },
  {
    type: 'text',
    label: 'What is the expected release date?',
    name: 'releaseDate',
    options: [],
  },
  {
    type: 'checkbox',
    label: 'What are the required features for the MVP (minimum viable product)?',
    name: 'features',
    options: featureOptions,
  },
  {
    type: 'checkbox',
    label: 'What is your target audience (choose all that apply)?',
    name: 'audience',
    options: audienceOptions,
  },
  {
    type: 'file',
    label: 'Upload desired application icon',
    name: 'appIcon',
    options: [],
  },
];