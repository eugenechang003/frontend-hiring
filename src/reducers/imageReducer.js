import { CLEAR_IMAGE, SET_IMAGE } from "../actions/types";

const initialState = {
  imageUrl: '',
}

const imageReducer = function(state=initialState, action) {
  switch (action.type) {
    case CLEAR_IMAGE:
      return {
        ...state,
        imageUrl: '',
      };
    case SET_IMAGE:
      return {
        ...state,
        imageUrl: action.payload,
      };
    default:
      return state;
  }
}

export default imageReducer;