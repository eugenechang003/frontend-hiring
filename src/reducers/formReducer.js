import { SUBMIT_FORM } from "../actions/types";

const initialState = {
  questionnaires: [],
}

const formReducer = function(state=initialState, action) {
  switch (action.type) {
    case SUBMIT_FORM:
      return {
        ...state,
        questionnaires: [...state.questionnaires, action.payload],
      }
    default:
      return state;
  }
}

export default formReducer;