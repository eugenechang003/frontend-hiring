import { combineReducers } from 'redux';
import { reducer as reduxForm } from 'redux-form';
import imageReducer from './imageReducer';
import formReducer from './formReducer';

// using redux form to handle form value state
export default combineReducers({
  form: reduxForm,
  image: imageReducer,
  questionnaire: formReducer,
});