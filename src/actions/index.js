import { CLEAR_IMAGE, SET_IMAGE, SUBMIT_FORM } from './types';

export const clearImage = () => ({
  type: CLEAR_IMAGE, payload: '',
});

export const setImage = base64 => ({
  type: SET_IMAGE, payload: base64 
});

export const submitForm = (values) => ({
  type: SUBMIT_FORM, payload: values
});